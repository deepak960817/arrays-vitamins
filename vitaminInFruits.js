const items = require('./arrays-vitamins.js');

function vitamins(store, current)
{
    current["contains"] = (current["contains"]).split(',');

    if((current["contains"]).length > 1)
    {
        current["contains"][1] = (current["contains"][1]).trim();
        
        if(store[current["contains"][0]])
        {
            store[current["contains"][0]].push(current["name"]);
        }
        else
        {
            store[current["contains"][0]] = [];
            store[current["contains"][0]].push(current["name"]);
        }

        if(store[current["contains"][1]])
        {
            store[current["contains"][1]].push(current["name"]);
        }
        else
        {
            store[current["contains"][1]] = [];
            store[current["contains"][1]].push(current["name"]);
        }
    }
    else
    {   
        if(store[current["contains"][0]])
        {
            store[current["contains"][0]].push(current["name"]);
        }
        else
        {
            store[current["contains"][0]] = [];
            store[current["contains"][0]].push(current["name"]);
        }
    }
    return store;
}

const result = items.reduce(vitamins, {});
console.log(result);