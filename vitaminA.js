const items = require('./arrays-vitamins.js');

function available(fruit)
{
    return fruit["contains"].includes("Vitamin A");
}

const result = items.filter(available);
console.log(result);