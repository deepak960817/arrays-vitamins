const items = require('./arrays-vitamins.js');

function available(fruit)
{
    return fruit.contains == "Vitamin C";
}

const result = items.filter(available);
console.log(result);